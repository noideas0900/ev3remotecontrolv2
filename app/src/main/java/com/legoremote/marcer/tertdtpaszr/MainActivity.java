package com.legoremote.marcer.tertdtpaszr;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;

import lejos.hardware.Audio;
import lejos.remote.ev3.RemoteRequestEV3;
import lejos.robotics.RegulatedMotor;

/**
 * <h1>Lego Remotesteuerung</h1>
 * The Lego Remotesteuerung program implements an application to control the
 * Lego EV3 via wifi from an mobile device with Android Version 7+
 * <br>
 * The {@link MainActivity} implemnts the main display.
 * Buttons and Textfields will be created if the app is starting.
 * It has already implemented an OnClickListener to check which button is pressed.
 */

public class MainActivity extends Activity implements OnClickListener {

    private RemoteRequestEV3 ev3;
    private RegulatedMotor left, right, crane;
    public static SeekBar speed;
    public static Integer engineSpeed = 200;
    private ImageButton connect;
    private Audio audio;

    /**
     * @param savedInstanceState Will be called if the app is starting.
     *                           Will be create buttons, textfield and the seekbar.
     *                           Add the listener to the components.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton left = (ImageButton) findViewById(R.id.Left);
        ImageButton right = (ImageButton) findViewById(R.id.Right);
        ImageButton stop = (ImageButton) findViewById(R.id.Stop);
        ImageButton forward = (ImageButton) findViewById(R.id.Up);
        ImageButton backward = (ImageButton) findViewById(R.id.Down);
        ImageButton rotateright = (ImageButton) findViewById(R.id.RotateRight);
        ImageButton rotateleft = (ImageButton) findViewById(R.id.RotateLeft);
        ImageButton raise = (ImageButton) findViewById(R.id.Raise);
        ImageButton lower = (ImageButton) findViewById(R.id.Lower);
        speed = (SeekBar) findViewById(R.id.Speed);
        connect = (ImageButton) findViewById(R.id.IpBtn);
        left.setOnClickListener(this);
        right.setOnClickListener(this);
        stop.setOnClickListener(this);
        forward.setOnClickListener(this);
        backward.setOnClickListener(this);
        rotateleft.setOnClickListener(this);
        rotateright.setOnClickListener(this);
        raise.setOnClickListener(this);
        lower.setOnClickListener(this);
        connect.setOnClickListener(this);
        speed.setOnSeekBarChangeListener(new SeekBarListener());
    }

    /**
     * @param v The activated view.
     *          Check which view was clicked and call the commands.
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.RotateLeft) new Control().execute("rotate left");
        else if (v.getId() == R.id.RotateRight) new Control().execute("rotate right");
        else if (v.getId() == R.id.Stop) new Control().execute("stop");
        else if (v.getId() == R.id.Right) new Control().execute("right");
        else if (v.getId() == R.id.Left) new Control().execute("left");
        else if (v.getId() == R.id.Up) new Control().execute("forward");
        else if (v.getId() == R.id.Down) new Control().execute("backward");
        else if (v.getId() == R.id.Raise) new Control().execute("raise");
        else if (v.getId() == R.id.Lower) new Control().execute("lower");
        else if (v.getId() == R.id.IpBtn) {
            /*Check if the connection does already exist and start a new connection if the robot is not
            * connected at the moment*/
            if (ev3 == null) {
                EditText inputIP = (EditText) findViewById(R.id.IpTxt);
                new Control().execute("connect", inputIP.getText().toString());
                //make toast
                Toast.makeText(MainActivity.this, "Trying to connect to EV3...", Toast.LENGTH_LONG).show();
            } else {
                new Control().execute("disconnect");
                //make toast
                Toast.makeText(MainActivity.this, "Disconnecting", Toast.LENGTH_LONG).show();
            }
        }
    }

    /**
     * This class is used to control the EV3.<br>
     * doInBackground is an function which will be called if Control.exceute(string) was called.
     * After doInBackground is finished onPostExecute will be called automatically.<br>
     * It will show a popup "Could not connect to EV3" if the app cannot connect.
     * Moreover it will show a popup if the app is not connected to EV3.
     */
    private class Control extends AsyncTask<String, Integer, Long> {

        protected Long doInBackground(String... cmd) {

            if (cmd[0].equals("connect")) {
                try {
                    ev3 = new RemoteRequestEV3(cmd[1]);
                    left = ev3.createRegulatedMotor("A", 'L');
                    right = ev3.createRegulatedMotor("B", 'L');
                    crane = ev3.createRegulatedMotor("D", 'L');
                    audio = ev3.getAudio();
                    audio.systemSound(3);
                    return 0l;
                } catch (IOException e) {
                    return 1l;
                }
            } else if (cmd[0].equals("disconnect") && ev3 != null) {
                audio.systemSound(2);
                left.close();
                right.close();
                ev3.disConnect();
                ev3 = null;
                return 0l;
            }

            if (ev3 == null) return 2l;

            ev3.getAudio().systemSound(1);

            if (cmd[0].equals("stop")) {
                left.stop(true);
                right.stop(true);
                crane.stop(true);
            } else if (cmd[0].equals("forward")) {
                left.setSpeed(engineSpeed);
                right.setSpeed(engineSpeed);
                left.forward();
                right.forward();
            } else if (cmd[0].equals("backward")) {
                left.setSpeed(engineSpeed);
                right.setSpeed(engineSpeed);
                left.backward();
                right.backward();
            } else if (cmd[0].equals("rotate left")) {
                left.setSpeed(engineSpeed);
                right.setSpeed(engineSpeed);
                left.backward();
                right.forward();
            } else if (cmd[0].equals("rotate right")) {
                left.setSpeed(engineSpeed);
                right.setSpeed(engineSpeed);
                left.forward();
                right.backward();
            } else if (cmd[0].equals("right")) {
                left.setSpeed(engineSpeed + (engineSpeed / 2));
                right.setSpeed(engineSpeed);
                left.forward();
                right.forward();
            } else if (cmd[0].equals("left")) {
                left.setSpeed(engineSpeed);
                right.setSpeed(engineSpeed + (engineSpeed / 2));
                left.forward();
                right.forward();
            } else if (cmd[0].equals("raise")) {
                crane.rotate(30);
            } else if (cmd[0].equals("lower")) {
                crane.rotate(-30);
            }

            return 0l;
        }

        protected void onPostExecute(Long result) {
            if (result == 1l)
                Toast.makeText(MainActivity.this, "Could not connect to EV3", Toast.LENGTH_LONG).show();
            else if (result == 2l)
                Toast.makeText(MainActivity.this, "Not connected", Toast.LENGTH_LONG).show();
        }
    }

}
