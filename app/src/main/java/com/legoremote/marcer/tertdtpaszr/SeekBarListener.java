package com.legoremote.marcer.tertdtpaszr;

import android.widget.SeekBar;

/**
 * Created by Ma on 12.10.2017.
 */

public class SeekBarListener implements SeekBar.OnSeekBarChangeListener {

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        MainActivity.engineSpeed = 200 * MainActivity.speed.getProgress();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
